import React from 'react'
import ListShoe from './ListShoe'
import { data_shoe } from './data_shoe'
import { useState } from 'react'
import ShoeCart from './ShoeCart';

function Ex_Shoe_Shop() {
    const [listShoe] = useState(data_shoe);
    const [shoeCart, setshoeCart] = useState([]);

    let themGioHang = (shoe) => {
        let shoeCartClone = [...shoeCart];
        let index = shoeCartClone.findIndex((item) => {
            return item.id === shoe.id;
        })

        if (index === -1) {
            let shoeClone = { ...shoe, soLuong: 1 };
            shoeCartClone.push(shoeClone);
        }

        else {
            shoeCartClone[index].soLuong += 1;
        }
        setshoeCart(shoeCartClone);
    }

    let xoaGioHang = (id) => {
        let shoeCartClone = [...shoeCart];
        let index = shoeCartClone.filter((item) => {
            return item.id !== id
        })
        setshoeCart(index);

    }

    let chinhSoLuong = (id, luaChon) => {
        let shoeCartClone = [...shoeCart];
        let index = shoeCartClone.findIndex((item) => {
            return item.id === id
        })

        shoeCartClone[index].soLuong = shoeCartClone[index].soLuong + luaChon;
        setshoeCart(shoeCartClone)
    }


    return (
        <div>
            <ShoeCart shoeCart={shoeCart} xoaGioHang={xoaGioHang} chinhSoLuong={chinhSoLuong}></ShoeCart>
            <ListShoe listShoe={listShoe} themGioHang={themGioHang}></ListShoe>
        </div>
    )
}

export default Ex_Shoe_Shop