import React from "react";

function ShoeCart({ shoeCart, xoaGioHang, chinhSoLuong }) {
    let renderShoeCart = () => {
        return shoeCart.map((shoe, index) => {
            return (
                <tr key={index}>
                    <td>{shoe.id}</td>
                    <td>{shoe.name}</td>
                    <td>{shoe.price}</td>
                    <td>
                        <button onClick={() => {
                            chinhSoLuong(shoe.id, -1)
                        }} className="btn btn-outline-info btn-sm">-</button>
                        <strong className='mx-2'>{shoe.soLuong}</strong>
                        <button onClick={() => {
                            chinhSoLuong(shoe.id, +1)
                        }} className="btn btn-outline-info btn-sm">+</button>
                    </td>
                    <td>{shoe.price * shoe.soLuong}</td>
                    <td><img src={shoe.image} alt='shoe_img' style={{ width: '50px' }} /></td>
                    <td>
                        <button onClick={() => {
                            xoaGioHang(shoe.id);
                        }} className="btn btn-danger">Xóa</button>
                    </td>

                </tr>
            )
        })
    }

    return (
        <div className="container p-5">
            <table className="table table-inverse table-inverse">
                <thead className="thead-inverse">
                    <tr>
                        <th>ID</th>
                        <th>Tên Sản phẩm</th>
                        <th>Gía</th>
                        <th>Số lượng</th>
                        <th>Tổng</th>
                        <th>Hình ảnh</th>
                        <th>Chức năng</th>

                    </tr>
                </thead>
                <tbody>
                    {renderShoeCart()}
                </tbody>
            </table>
        </div>
    )


}

export default ShoeCart