import React from 'react'

function ItemShoe({ shoe, themGioHang }) {

    return (
        <div className='col-4 mb-5'>

            <img src={shoe.image} className="card-img-top" alt="..." />
            <div className="card-body">
                <h5 className="card-title">{shoe.name}</h5>
                <p className="card-text">{shoe.price}</p>
                <a onClick={() => {
                    themGioHang(shoe);
                }} href="#" className="btn btn-primary">Add to cart</a>
            </div>
        </div>
    )
}


export default ItemShoe
