import React from 'react'
import ItemShoe from './ItemShoe'

function ListShoe({ listShoe, themGioHang }) {
    return (
        <div className='container p-5'>

            <div className="row p-5">
                {listShoe.map((shoe, index) => {
                    return <ItemShoe key={index} shoe={shoe} themGioHang={themGioHang} />
                })}
            </div>


        </div>
    )
}

export default ListShoe
